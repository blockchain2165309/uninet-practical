# set PATH so it includes HLF bin if it exists

if [ -d "/workspaces/HyperLedgerFabric_Network/fabric-samples/bin" ] ; then
PATH="/workspaces/HyperLedgerFabric_Network/fabric-samples/bin:$PATH"
fi

# Delete existing artifacts
rm -rf ./crypto-config

#Generate Crypto artifacts for organizations
cryptogen generate --config=./crypto-config.yaml --output=./crypto-config/